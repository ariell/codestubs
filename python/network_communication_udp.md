# UDP network programming in Python

## UNICAST
#### Sender:
When creating a UDP socket, specify SOCK_DGRAM as the argument of the socket function. The sending side needs to specify send destination with sendto instead of send.

```python
from __future__ import print_function
import socket
import time
from contextlib import closing

def main():
  host = '127.0.0.1'
  port = 4000
  count = 0
  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  with closing(sock):
    while True:
      message = 'Hello world : {0}'.format(count).encode('utf-8')
      print(message)
      sock.sendto(message, (host, port))
      count += 1
      time.sleep(1)
  return

if __name__ == '__main__':
  main()
```

#### Receiver:
The recipient specifies the recipient by bind and receives data with recv.

```python
from __future__ import print_function
import socket
from contextlib import closing

def main():
  host = '127.0.0.1'
  port = 4000
  bufsize = 4096

  sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  with closing(sock):
    sock.bind((host, port))
    while True:
      print(sock.recv(bufsize))
  return

if __name__ == '__main__':
  main()
```

## Multicast
In the program on the sending side of multicast communication, set_option_option_socket option is set using setsockopt. Also, if IP_MULTICAST_LOOP is set to 0 (false), the multicast packet will not loop back to the local socket, so the overhead of receiving the packet sent by itself can be reduced.

#### Sender:

```python
from __future__ import print_function
import socket
import time
from contextlib import closing

def main():
  local_address   = '192.168.0.1' #
  multicast_group = '239.255.0.1' #
  port = 4000

  with closing(socket.socket(socket.AF_INET, socket.SOCK_DGRAM)) as sock:

    # sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 0)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(local_address))

    count = 0
    while True:
      message = 'Hello world : {0}'.format(count).encode('utf-8')
      print(message)
      sock.sendto(message, (multicast_group, port))
      count += 1
      time.sleep(0.5)
  return

if __name__ == '__main__':
  main()
```

#### Receiver:

In the receiving side program, it is necessary to set the IP_ADD_MEMBERSHIP socket option and join the multicast group. By setting SO_REUSEADDR to 1 (true) before bind, multicast can be received from multiple processes.

```python
from __future__ import print_function
import socket
from contextlib import closing

def main():
  local_address   = '192.168.0.2' #
  multicast_group = '239.255.0.1' #
  port = 4000
  bufsize = 4096

  with closing(socket.socket(socket.AF_INET, socket.SOCK_DGRAM)) as sock:
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', port))
    sock.setsockopt(socket.IPPROTO_IP,
                    socket.IP_ADD_MEMBERSHIP,
                    socket.inet_aton(multicast_group) + socket.inet_aton(local_address))

    while True:
      print(sock.recv(bufsize))
  return

if __name__ == '__main__':
  main()
```

**NOTE:** communication may not be possible if a firewall is enabled.
