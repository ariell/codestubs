# TCP Network programming in Python

#### Server:
On the server side,

1. Create socket with socket
1. Specify address and port number with bind
1. Wait for clients to connect with listen
1. Accept client's connection with accept
1. Send and receive client data using send or recv
1. Close the socket with close

```python
from __future__ import print_function
import socket
from contextlib import closing

def main():
  host = '127.0.0.1'
  port = 4000
  backlog = 10
  bufsize = 4096

  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  with closing(sock):
    sock.bind((host, port))
    sock.listen(backlog)
    while True:
      conn, address = sock.accept()
      with closing(conn):
        msg = conn.recv(bufsize)
        print(msg)
        conn.send(msg)
  return

if __name__ == '__main__':
  main()
```

#### Client:
On the client side, after creating the socket, connect to the server using connect and execute close after the communication is completed.

```python
from __future__ import print_function
import socket
from contextlib import closing

def main():
  host = '127.0.0.1'
  port = 4000
  bufsize = 4096

  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  with closing(sock):
    sock.connect((host, port))
    sock.send(b'Hello world')
    print(sock.recv(bufsize))
  return

if __name__ == '__main__':
  main()
```

## TCP communication using SELECT
In the above program on the server side, since the program blocks while accept waiting for client connection, it can not communicate with multiple clients at the same time. When you use select, you can monitor multiple sockets concurrently, so you can communicate with multiple clients at the same time.

#### Server:

```python
from __future__ import print_function
import socket
import select

def main():
  host = '127.0.0.1'
  port = 4000
  backlog = 10
  bufsize = 4096

  server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  readfds = set([server_sock])
  try:
    server_sock.bind((host, port))
    server_sock.listen(backlog)

    while True:
      rready, wready, xready = select.select(readfds, [], [])
      for sock in rready:
        if sock is server_sock:
          conn, address = server_sock.accept()
          readfds.add(conn)
        else:
          msg = sock.recv(bufsize)
          if len(msg) == 0:
            sock.close()
            readfds.remove(sock)
          else:
            print(msg)
            sock.send(msg)
  finally:
    for sock in readfds:
      sock.close()
  return

if __name__ == '__main__':
  main()
```

#### Client:

```python
from __future__ import print_function
import sys
import socket
from contextlib import closing

def main():
  host = '127.0.0.1'
  port = 4000
  bufsize = 4096

  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  with closing(sock):
    sock.connect((host, port))
    while True:
      line = sys.stdin.readline().rstrip()
      if len(line) == 0:
        break
      sock.send(line.encode('utf-8'))
      print(sock.recv(bufsize))
  return

if __name__ == '__main__':
  main()
```
