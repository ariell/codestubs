# Useful Code Stubs #

This repo contains useful, reusable codestubs

## Python
* [TCP communication](python/network_communication_tcp.md)
* [UDP communication](python/network_communication_udp.md)


## Bash
* [SSH tunneling/forwarding](bash/ssh_tunnelling.md)
* [GIT commands](bash/git_commands.md)
