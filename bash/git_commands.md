# Useful git commands

## Logging
To get a log of all commits as a csvfile:
```bash
git log  --oneline --pretty='"%h","%an","%ad","%s",'  > commits-$(basename `pwd`)-$(date +"%Y%m%d%H%M").csv
```

To get a log of all commits with changes as a csvfile since `2019-01-01`:
```bash
SINCE="2019-01-01"  # Set this to the date you want to query from
LOGFILE="commits-$(basename `pwd`)-$(date +"%Y%m%d%H%M").csv"
echo "#,user,datetime,message,files_changed,insertions,deletions" > ${LOGFILE}
git log --date=iso --since=$SINCE --oneline --pretty='@"%h","%an","%ad","%s",'  --stat   |grep -v \| |  tr -d "\n"   |  tr "@" "\n"  >> ${LOGFILE}
```
