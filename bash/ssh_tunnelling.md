# Useful ssh tunnel commands

### Port forwarding
To access port `8080` on a remote server `remote.ip` by forwarding to port `8081` on your local machine:
```bash
# From your local machine
ssh -L 8081:localhost:8080 remoteuser@remote.ip
```
Then, for example, on the local machine, you can access `http://remote.ip:8080` through `http://localhost:8081`


### Reverse ssh tunnel
To connect via SSH (port `22`) to a machine behind a firewall by setting up a reverse tunnel to a computer in your office or home `home.ip`.
```bash
# From remote machine running behind a firewall
ssh -R 2200:localhost:22 homeuser@home.ip
```
Note: this requires that `home.ip` is externally visible and has an SSH route. It usually involves setting an ssh port (`22`) forward on the home/office network.

Then, to ssh into remote machine behind firewall using the remote user `remoteuser`, type the following on the computer in your office or home:
```bash
ssh -p 2200 remoteuser@localhost
```
